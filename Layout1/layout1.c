#include <gtk/gtk.h>
#include <gdk/gdk.h>

GtkWidget *status_bar;

static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print ("Hello World\n");
}

static void push_item( GtkWidget *widget,
                       gpointer   data )
{
  static int count = 1;
  gchar *buff;

  buff = g_strdup_printf ("Item %d", count++);
  gtk_statusbar_push (GTK_STATUSBAR (status_bar), GPOINTER_TO_INT (data), buff);
  g_free (buff);
}

int
main( int    argc,
      char **argv )
{
    GtkBuilder *builder;
    GtkWidget  *window;
    GtkWidget *button;

    guint *context_id;
    //Data        data;

    gtk_init( &argc, &argv );

    /* Create builder and load interface */
    builder = gtk_builder_new();
    gtk_builder_add_from_file( builder, "layout1.glade", NULL );

    /* Obtain widgets that we need */
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    button = GTK_WIDGET(gtk_builder_get_object(builder, "button1"));
    status_bar = GTK_WIDGET(gtk_builder_get_object( builder, "statusbar1"));

    /* Get Context ID for Status Bar */
    context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_bar), "Statusbar example");
    
    //data.quit = GTK_WIDGET( gtk_builder_get_object( builder, "dialog1" ) );
    //data.about = GTK_WIDGET( gtk_builder_get_object( builder, "aboutdialog1" ) );

    /* Connect callbacks */
    //gtk_builder_connect_signals( builder, &data );
    //g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);
    g_signal_connect (button, "clicked",G_CALLBACK (push_item), GINT_TO_POINTER (context_id));
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    /* Destroy builder */
    g_object_unref( G_OBJECT( builder ) );

    /* Show main window and start main loop */
    gtk_widget_show( window );
    //gtk_window_fullscreen (window);
    //gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(gtk_window)),gdk_cursor_new(GDK_BLANK_CURSOR));
    gtk_main();

    return( 0 );
}
