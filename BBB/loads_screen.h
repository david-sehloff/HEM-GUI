#ifndef _LOADSH_
#define _LOADSH_

#define NUM_LOADS 8
#define LOAD_CHARS 10
#define PWR_CHARS 8



//global struct for each node's meter values
struct node_data{
  float voltage;
  float current;
  float power;
  bool on;
};

struct node_data load[NUM_LOADS];

/* stores the value of the relays for testing - REPLACE WITH ACTUAL HEMAPP RELAY STATUS */
bool relayON[NUM_LOADS];

/* struct to store all the Gtk elements specific to the home screen */
typedef struct Loads_Private LoadsPrivate;
struct Loads_Private
{
    GtkBuilder *builder;
    GtkWidget *window;
    GtkWidget *button;
    GtkScrolledWindow *scrollwindow;
	GtkWidget *updatebutton;
	GtkWidget *homebutton;
    GtkWidget *grid;
    GtkWidget *label_load[NUM_LOADS];
    GtkWidget *relay_toggle_box;
	GtkWidget *relay_toggle[NUM_LOADS];
	GtkWidget *status_load[NUM_LOADS];
	GtkWidget *time_label;
    gint timeout_tag;
};

LoadsPrivate* loads_priv;

#define UI_FILE_LOADS "loads.ui"
#define LOADS_WINDOW "loads_window"


void
on_home_button_clicked (GtkWidget *widget, gpointer data);

GtkWidget*
create_load_window (void);

void
relay_toggled(GtkWidget *toggle, gpointer num);

void
on_loadupdate_button_clicked (GtkWidget *widget, gpointer data);

int
update_load_data(gpointer loads_priv);

int
relay_update(gpointer loads_priv);

int
print_load_data(gpointer loads_priv);

int
get_load_data(void);

GtkWidget*
create_home_window (void);

#endif
