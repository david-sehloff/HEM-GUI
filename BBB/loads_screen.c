/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * loads.c
 * Copyright (C) 2017 David Sehloff <dsehloff@wisc.edu>
 * 
 * hem-gui is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hem-gui is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <glib/gi18n.h>



//#include "settings_screen.h"
#include "home_screen.h"
#include "loads_screen.h"


/*
 * Function:  get_load_data
 * --------------------
 * Updates the load data stored in load[i] for each load i by pulling the associated HEMApp data value
 */
int
get_load_data(void)
{
  int success = 0;
  int i;
  for(i=0;i<NUM_LOADS;i++){
//TODO: Interface with HEMApp, pull actual data
    load[i].voltage=12;
    load[i].current=0;
    load[i].power=load[i].voltage*load[i].current;
    load[i].on=relayON[i];
  }
  load[2].power=12.42;
  load[3].power=7.94;
  load[1].power=3.29;
  success = 1;
  return success;
}

/*
 * Function:  print_load_data
 * --------------------
 * prints meaningful verbal representations of the current status variables to the statusbars
 */
int
print_load_data(gpointer loads_priv)
{
  int success = 0;
  gchar *buff;

  /* push to each load's power statusbar */
  int i;
  for(i=0;i<NUM_LOADS;i++){
    buff = g_strdup_printf ("%.2lf W",load[i].power);
    gtk_label_set_text (GTK_LABEL(((LoadsPrivate*)loads_priv)->status_load[i]),buff);
    g_free (buff);
  }
	//printf("%s\n",gtk_label_get_text ((GtkLabel*)(((LoadsPrivate*)loads_priv)->time_label)));
  update_time(GTK_WIDGET(((LoadsPrivate*)loads_priv)->time_label));
  success = 1;
  return success;
}


/*
 * Function:  relay_update
 * --------------------
 * Visually update display according to current relay statuses, changing label and CSS class for visual confirmation
 */
int
relay_update(gpointer loads_priv)
{
    int success = 0;
    GtkStyleContext *context;
	int i;
    for (i=0;i<NUM_LOADS;i++){
        /*CSS*/        
        context = gtk_widget_get_style_context(((LoadsPrivate*)loads_priv)->relay_toggle[i]);

        if (load[i].on){
            gtk_button_set_label(GTK_BUTTON(((LoadsPrivate*)loads_priv)->relay_toggle[i]),"ON");

			g_signal_handlers_block_by_func(((LoadsPrivate*)loads_priv)->relay_toggle[i], G_CALLBACK(relay_toggled), GINT_TO_POINTER(i));              
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(((LoadsPrivate*)loads_priv)->relay_toggle[i]), TRUE);
			g_signal_handlers_unblock_by_func(((LoadsPrivate*)loads_priv)->relay_toggle[i], G_CALLBACK(relay_toggled), GINT_TO_POINTER(i));

            /*CSS*/
            gtk_style_context_remove_class (context,"RelayOFF");
            gtk_style_context_add_class(context,"RelayON");
        }
        else {
            gtk_button_set_label(GTK_BUTTON(((LoadsPrivate*)loads_priv)->relay_toggle[i]),"OFF");

			g_signal_handlers_block_by_func(((LoadsPrivate*)loads_priv)->relay_toggle[i], G_CALLBACK(relay_toggled), GINT_TO_POINTER(i));              
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(((LoadsPrivate*)loads_priv)->relay_toggle[i]), FALSE);
			g_signal_handlers_unblock_by_func(((LoadsPrivate*)loads_priv)->relay_toggle[i], G_CALLBACK(relay_toggled), GINT_TO_POINTER(i));

            /*CSS*/
            gtk_style_context_remove_class (context,"RelayON");
            gtk_style_context_add_class(context,"RelayOFF");
        }
    }
	success = 1;
    return success;
}




/*
 * Function:  update_load_data
 * --------------------
 * update latest values of stored variables in load[i],
 * print them to labels,
 * visually update relay toggles
 */
int
update_load_data(gpointer loads_priv)
{ 
  int success=0; 
  if(get_load_data() && print_load_data(loads_priv) && relay_update(loads_priv))
  {
	success=1;
  }
  return success;
}

/*
 * Function:  on_loadupdate_button_clicked
 * --------------------
 * To be called when update button is clicked,
 * passes the Private Load variables into update_load_data to be updated
 */
void
on_loadupdate_button_clicked (GtkWidget *widget, gpointer data)
{
	update_load_data(data);
}

/*
 * Function:  relay_toggled
 * --------------------
 * To be called when relay (num) is toggled,
 * passes the 0-indexed number (num) of the relay to the HEMApp function to change the status of that relay
 */
void
relay_toggled(GtkWidget *toggle, gpointer num)
{
    int i = GPOINTER_TO_INT (num);
//TODO: Interface with HEMApp to change relay value
    relayON[i]=!relayON[i];
    //printf("relay %d: %s\n",i, relayON[i] ? "ON" : "OFF");    //debugging
    //printf("relay %d variable: %s\n",i, load[i].on ? "ON" : "OFF");     //debugging
	update_load_data((gpointer)loads_priv);
}

/*
 * Function:  on_home_button_clicked
 * --------------------
 * To be called when home button clicked,
 * switches the screen to the home screen
 * by hiding the loads screen, freeing its variables, and presenting the home window
 */
void
on_home_button_clicked (GtkWidget *widget, gpointer data)
{
	gtk_widget_hide(GTK_WIDGET(((LoadsPrivate*)data)->window));
	g_source_remove ((gint)((LoadsPrivate*)data)->timeout_tag);
	g_free ((LoadsPrivate*)data);	
	gtk_window_present((GtkWindow*)home_priv->window);
}

/*
 * Function:  create_load_window
 * --------------------
 * Sets up the full window for displaying loads, pulls from *.ui file,
 * creates other widgets, and connects functions and widgets,
 * returns the window
 */
GtkWidget*
create_load_window (void)
{
    /*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    GtkStyleContext *context;
    /*---------------------------*/


	/* Get memory for structure of this window's widgets */
	loads_priv = g_malloc (sizeof (struct Loads_Private));

	GError* error = NULL;

	/* Load UI from file */
	loads_priv->builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (loads_priv->builder, UI_FILE_LOADS, &error))
	{
		g_critical ("Couldn't load builder file: %s", error->message);
		g_error_free (error);
	}

	/* Get the window object from the ui file */
	loads_priv->window = GTK_WIDGET (gtk_builder_get_object (loads_priv->builder, LOADS_WINDOW));
        if (!loads_priv->window)
        {
                g_critical ("Widget \"%s\" is missing in file %s.",
				LOADS_WINDOW,
				UI_FILE_LOADS);
        }
	loads_priv->scrollwindow = (GtkScrolledWindow*)(gtk_builder_get_object(loads_priv->builder, "scrolledwindow"));
	gtk_widget_set_vexpand((GtkWidget *)loads_priv->scrollwindow,TRUE);
	loads_priv->updatebutton = GTK_WIDGET (gtk_builder_get_object (loads_priv->builder, "update_button"));
	loads_priv->homebutton = GTK_WIDGET (gtk_builder_get_object (loads_priv->builder, "home_button"));

	loads_priv->time_label = GTK_WIDGET(gtk_builder_get_object(loads_priv->builder, "time_label"));
	//printf("%s\n",gtk_label_get_text ((GtkLabel*)(((LoadsPrivate*)loads_priv)->time_label)));
	//gtk_label_set_text (GTK_LABEL (loads_priv->time_label),"new label");
	//printf("%s\n",gtk_label_get_text ((GtkLabel*)(((LoadsPrivate*)loads_priv)->time_label)));
    //set the name of each load for the label
    char load_names[NUM_LOADS][LOAD_CHARS+1];
	int i;
    for(i=0;i<NUM_LOADS;i++){
        sprintf(load_names[i],"Load %d",i+1);
    };

    /* Add a grid to the scrolled window */
    loads_priv->grid=gtk_grid_new();
    gtk_container_add (GTK_CONTAINER(loads_priv->scrollwindow), loads_priv->grid);
    for(i=0;i<NUM_LOADS;i++){
		
        loads_priv->label_load[i]=gtk_label_new(load_names[i]);
        gtk_label_set_width_chars (GTK_LABEL(loads_priv->label_load[i]),LOAD_CHARS);
        gtk_label_set_max_width_chars (GTK_LABEL(loads_priv->label_load[i]),LOAD_CHARS);
        gtk_widget_set_size_request (loads_priv->label_load[i],60,40);

        loads_priv->status_load[i]=gtk_label_new(load_names[i]);
        gtk_label_set_width_chars (GTK_LABEL(loads_priv->status_load[i]),PWR_CHARS);
        gtk_label_set_max_width_chars (GTK_LABEL(loads_priv->status_load[i]),PWR_CHARS);
        
        //context_id[i] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load[i]), "Power");
        loads_priv->relay_toggle_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
        loads_priv->relay_toggle[i] = gtk_toggle_button_new_with_label("OFF");
        gtk_toggle_button_set_mode((GtkToggleButton*)(loads_priv->relay_toggle[i]),FALSE);
        gtk_container_add (GTK_CONTAINER(loads_priv->relay_toggle_box),loads_priv->relay_toggle[i]);
        gtk_widget_set_size_request (loads_priv->relay_toggle[i],40,35);
        gtk_widget_set_size_request (loads_priv->relay_toggle_box,40,40);

        /*--CSS--*/
        context = gtk_widget_get_style_context(loads_priv->label_load[i]);
        gtk_style_context_add_class(context,"LoadLabel");
        context = gtk_widget_get_style_context(loads_priv->status_load[i]);
        gtk_style_context_add_class(context,"LoadStatus");
        context = gtk_widget_get_style_context(loads_priv->relay_toggle[i]);
        if (load[i].on){
            gtk_style_context_add_class(context,"RelayON");
        }
        else {
            gtk_style_context_add_class(context,"RelayOFF");
        }
        /*-------*/

        /* attach the label and status bar for each load to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)loads_priv->grid, loads_priv->label_load[i],0,i,1,1);
        gtk_grid_attach ((GtkGrid*)loads_priv->grid, loads_priv->status_load[i],2,i,1,1);
        gtk_grid_attach ((GtkGrid*)loads_priv->grid, loads_priv->relay_toggle_box,1,i,1,1);
        /* Connect callbacks */
        g_signal_connect (loads_priv->relay_toggle[i], "toggled", G_CALLBACK (relay_toggled),GINT_TO_POINTER(i));
    }
	
    /* Connect callbacks */
    g_signal_connect (loads_priv->updatebutton, "clicked",G_CALLBACK (on_loadupdate_button_clicked),(gpointer)loads_priv);
	g_signal_connect (loads_priv->homebutton, "clicked",G_CALLBACK (on_home_button_clicked),(gpointer)loads_priv);
    g_signal_connect (loads_priv->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

	/* Initialize values of widgets */
	update_load_data((gpointer)loads_priv);


	/* Start Periodic Updates */
	loads_priv->timeout_tag=g_timeout_add(UPDATE_INTERVAL_MS, (GSourceFunc)update_load_data, (gpointer)loads_priv);


    /* -----------------CSS----------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /*--------------------------------------------------------------------------------------------------------*/
    
    /* For touchscreen, set fullscreen */
    gtk_window_fullscreen ((GtkWindow*)loads_priv->window);

    /* Destroy builder */
    g_object_unref( G_OBJECT( loads_priv->builder ) );

	return loads_priv->window;
}

