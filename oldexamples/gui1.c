#include <gtk/gtk.h>

/* Construct a GtkBuilder instance and load our UI description */
//GtkBuilder *builder;
//builder = gtk_builder_new ();
//gtk_builder_add_from_file (builder, "gui2.ui", NULL);

static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print ("Settings\n");
}


int
display_status (const gchar *name,
		GtkBuilder *builder, 
		const gchar *text,
		gint offset)
{
  GObject *buffer;
  GtkTextIter iter;
  buffer=gtk_builder_get_object (builder,name);
  if (offset<0){
    offset = gtk_text_buffer_get_char_count ((GtkTextBuffer*) buffer);
  } 
  gtk_text_buffer_get_iter_at_offset ((GtkTextBuffer*) buffer,&iter,offset);
  //text=" "+text+"\0";
  gtk_text_buffer_insert ((GtkTextBuffer*) buffer,&iter,text,-1);
  return offset;
}

int
main (int   argc,
      char *argv[])
{
  GtkBuilder *builder;
  GObject *window;
  GObject *button;

  gtk_init (&argc, &argv);

  /* Construct a GtkBuilder instance and load our UI description */
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "gui2.ui", NULL);

gint hemo=display_status("hembuf",builder," running\0",-1);
gint wifio=display_status("wifibuf",builder," eduroam signal 70/70\0",-1);
gint gsmo=display_status("gsmbuf",builder," connected\0",-1);
gint mobo=display_status("mobiledatabuf",builder," connected\0",-1);
gint i2co=display_status("I2Cbuf",builder," I2C1, I2C2 enabled\0",-1);
gint uarto=display_status("uartbuf",builder," enabled\0",-1);
gint cldo=display_status("cloudbuf",builder," connected\0",-1);
gint rly1o=display_status("relay1",builder," ON, 3.2W\0",-1);

display_status("hembuf",builder," running1\0",hemo);
display_status("wifibuf",builder," eduroam signal 70/70\0",wifio);
display_status("gsmbuf",builder," connected\0",gsmo);
display_status("mobiledatabuf",builder," connected\0",mobo);
display_status("I2Cbuf",builder," I2C1, I2C2 enabled\0",i2co);
display_status("uartbuf",builder," enabled\0",uarto);
display_status("cloudbuf",builder," connected\0",cldo);
display_status("relay1",builder," ON, 3.1W\0",rly1o);

  /* Connect signal handlers to the constructed widgets. */
  window = gtk_builder_get_object (builder, "window");
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  button = gtk_builder_get_object (builder, "settings");
  g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);

  gtk_main ();


  //gtk_main ();

  return 0;
}

