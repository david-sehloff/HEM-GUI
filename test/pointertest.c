#include <stdio.h>
#include <math.h>

int main()
{ 
    double x;
    //double y;            /* A normal integer*/
    double *p;           /* A pointer to an integer ("*p" is an integer, so p
                       must be a pointer to an integer) */

    //p = &y;           /* Read it, "assign the address of x to p" */
    //scanf( "%lf", &x );          /* Put a value in x, we could also use p here */
    //p = &x;
    //printf( "%lf\n", *p ); /* Note the use of the * to get the value */
    //getchar();

    int a = 5;
    int b = a;
    b++;
    printf("a=%d\n",a);
}
