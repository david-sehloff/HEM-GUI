
#ifndef _HOMEH_
#define _HOMEH_


#define NAME_CHARS 14
#define VAL_CHARS 15
#define NUM_ENTRIES 7

#define UPDATE_INTERVAL_MS 1000

typedef struct status_entry{
    char name[NAME_CHARS];
    char status[VAL_CHARS];
	void (*update)(int i);
	bool levelbar;
	double level;
} status_entry;

//struct for time
struct time_val{
  int hour;
  int minute;
};

struct time_val current_time;


/* struct to store all the Gtk elements specific to the home screen */
typedef struct Home_Private HomePrivate;
struct Home_Private
{
	GtkWidget *window;
	GtkBuilder *builder;
	GtkScrolledWindow *scrollwindow;
	GtkWidget *updatebutton;
	GtkWidget *loadbutton;
    GtkWidget *settingsbutton;
	GtkWidget *levelbargrid;
	GtkWidget *grid;
    GtkWidget *status_label[NUM_ENTRIES];
	GtkWidget *status_val[NUM_ENTRIES];
	GtkWidget *levelbar[NUM_ENTRIES];
	GtkWidget *time_label;
    gint timeout_tag;
};


HomePrivate* home_priv;


/* For testing purpose, define TEST to use the local (not installed) ui file */

#define UI_FILE_HOME "home.ui"

#define HOME_WINDOW "home_window"



status_entry status_table[NUM_ENTRIES];

void
on_settings_button_clicked (GtkWidget *widget, gpointer data);

void
on_load_button_clicked (GtkWidget *widget, gpointer data);

void
on_update_button_clicked (GtkWidget *widget, gpointer data);

void
on_window_destroy (GtkWidget *widget, gpointer data);

int
update_and_print_status(gpointer home_priv);

void
update_time(GtkWidget *timelabel);

void
populate_table(void);

void
update_memcloud(int i);

void 
update_uart(int i);

void
update_i2c(int i);

void
update_mobiledata(int i);

void
update_gsm(int i);

void
update_wifi(int i);

void
update_hemapp(int i);

#endif

