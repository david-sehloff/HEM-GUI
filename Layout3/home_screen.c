/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * home.c
 * Copyright (C) 2017 David Sehloff <dsehloff@wisc.edu>
 * 
 * hem-gui is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hem-gui is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <glib/gi18n.h>

#include "settings_screen.h"
#include "home_screen.h"
#include "loads_screen.h"


/* -------------specific update functions for status values--------------- */
//TODO: Interface with HEMApp and connection manager to pull the values of each
void
update_hemapp(int i)
{
	if(true){
		sprintf(status_table[i].status,"running");
	}
	else{
		sprintf(status_table[i].status,"not running");
	}
}
void
update_wifi(int i)
{
	sprintf(status_table[i].status,"eduroam");
	status_table[i].level=64/70.0;
}
void
update_gsm(int i)
{
	if(true){
		sprintf(status_table[i].status,"connected");
	}
	else{
		sprintf(status_table[i].status,"not connected");
	}
}
void
update_mobiledata(int i)
{
	if(true){
		sprintf(status_table[i].status,"connected");
	}
	else{
		sprintf(status_table[i].status,"not connected");
	}
}
void
update_i2c(int i)
{
	if(true){
		sprintf(status_table[i].status,"I2C1,I2C2");
	}
	else{
		sprintf(status_table[i].status,"not connected");
	}
}
void 
update_uart(int i)
{
	if(true){
		sprintf(status_table[i].status,"enabled");
	}
	else{
		sprintf(status_table[i].status,"not enabled");
	}
}
void
update_memcloud(int i)
{
	if(true){
		sprintf(status_table[i].status,"connected");
	}
	else{
		sprintf(status_table[i].status,"not connected");
	}
}
/* ---------------------------------------------------------------- */

/*
 * Function:  populate_table
 * --------------------
 * Fills the status_table with details for each entry
 * Gives the name label, update function to call, and whether or not a levelbar is displayed
 * for each status entry on the home screen
 */
void
populate_table(void)
{
	sprintf(status_table[0].name,"HEMApp");
	status_table[0].update=update_hemapp;
	status_table[0].levelbar=false;

	sprintf(status_table[1].name,"Wi-Fi Network");
	status_table[1].update=update_wifi;
	status_table[1].levelbar=true;

	sprintf(status_table[2].name,"GSM Status");
	status_table[2].update=update_gsm;
	status_table[2].levelbar=false;

	sprintf(status_table[3].name,"Mobile Data");
	status_table[3].update=update_mobiledata;
	status_table[3].levelbar=false;

	sprintf(status_table[4].name,"I2C");
	status_table[4].update=update_i2c;
	status_table[4].levelbar=false;

	sprintf(status_table[5].name,"UART");
	status_table[5].update=update_uart;
	status_table[5].levelbar=false;

	sprintf(status_table[6].name,"MEMCloud");
	status_table[6].update=update_memcloud;
	status_table[6].levelbar=false;
}

/*
 * Function:  update_time
 * --------------------
 * Time update function called by all screens to set the text of the time label
 */
void
update_time(GtkWidget *timelabel)
{
	gchar *buff;
    gchar *hourstr;
	gchar *minutestr;
//TODO: Interface with RTC in HEMApp to get actual system time
	current_time.hour=7;
	current_time.minute=2;

	if (current_time.hour<10){
		hourstr = g_strdup_printf ("0%d",current_time.hour);
	}
	else {
		hourstr = g_strdup_printf ("%d",current_time.hour);
	}
	if (current_time.minute<10){
		minutestr = g_strdup_printf ("0%d",current_time.minute);
	}
	else {
		minutestr = g_strdup_printf ("%d",current_time.minute);
	}
	buff = g_strdup_printf ("%s:%s",hourstr,minutestr);
	g_free (hourstr);
	g_free (minutestr);
	//printf("%s\n",buff);
	gtk_label_set_text (GTK_LABEL (timelabel),buff);
	g_free (buff);

}

/*
 * Function:  update_and_print_status
 * --------------------
 * Single function to update status variables for entire status table
 * and then set the status_val label for each
 * Also calls update_time
 */
int
update_and_print_status(gpointer home_priv)
{	
	int success = 0;
	gchar *buff;
	for (int i=0;i<NUM_ENTRIES;i++){
		status_table[i].update(i);
		buff = g_strdup_printf ("%s",status_table[i].status);
		gtk_label_set_text (GTK_LABEL(((HomePrivate*)home_priv)->status_val[i]),buff);
		g_free (buff);
		if (status_table[i].levelbar)
		{
			gtk_level_bar_set_value ((GtkLevelBar*)(((HomePrivate*)home_priv)->levelbar[i]),status_table[i].level);
		}
	}
	update_time(((HomePrivate*)home_priv)->time_label);
	success = 1;
	return success;
}

/* Signal handlers */

/*
 * Function:  on_window_destroy
 * --------------------
 * Called when the window is closed, quits gtk
 */
void
on_window_destroy (GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}

/*
 * Function:  on_update_button_clicked
 * --------------------
 * Called when the update button is clicked, updates and prints status
 */
void
on_update_button_clicked (GtkWidget *widget, gpointer data)
{
	update_and_print_status(data);
}

/*
 * Function:  on_load_button_clicked
 * --------------------
 * Called when the load button is clicked,
 * hides home window and creates and shows loads window
 * Does not free Private Home variables
 */
void
on_load_button_clicked (GtkWidget *widget, gpointer data)
{
	GtkWidget *load_window;
	//g_free (home_priv);	
	load_window=create_load_window();
	gtk_widget_show_all (load_window);
	gtk_widget_hide(GTK_WIDGET(((HomePrivate*)data)->window));
}

/*
 * Function:  on_settings_button_clicked
 * --------------------
 * Called when the settings button is clicked,
 * hides home window and creates and shows settings window
 * Does not free Private Home variables
 */
void
on_settings_button_clicked (GtkWidget *widget, gpointer data)
{
	GtkWidget *settings_window;
	//g_free (home_priv);	
	settings_window=create_settings_window();
	gtk_widget_show_all (settings_window);
	gtk_widget_hide(GTK_WIDGET(((HomePrivate*)data)->window));
}

/*
 * Function:  create_home_window
 * --------------------
 * Sets up the full home window for displaying status, pulls from *.ui file,
 * creates other widgets, and connects functions and widgets,
 * returns the window as a GtkWidget pointer
 */
GtkWidget*
create_home_window (void)
{
	/*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    //GtkStyleContext *context;
    /*---------------------------*/


	/* Get memory for structure of this window's widgets */
	home_priv = g_malloc (sizeof (struct Home_Private));

	GError* error = NULL;

	populate_table();

	/* Load UI from file */
	home_priv->builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (home_priv->builder, UI_FILE_HOME, &error))
	{
		g_critical ("Couldn't load builder file: %s", error->message);
		g_error_free (error);
	}

	/* Auto-connect signal handlers */
	//gtk_builder_connect_signals (builder, NULL);

	/* Get the window object from the ui file */
	home_priv->window = GTK_WIDGET (gtk_builder_get_object (home_priv->builder, HOME_WINDOW));
        if (!home_priv->window)
        {
                g_critical ("Widget \"%s\" is missing in file %s.",
				HOME_WINDOW,
				UI_FILE_HOME);
        }
	home_priv->scrollwindow = (GtkScrolledWindow*)(gtk_builder_get_object(home_priv->builder, "scrolledwindow"));
	gtk_widget_set_vexpand((GtkWidget *)home_priv->scrollwindow,TRUE);
	home_priv->updatebutton = GTK_WIDGET (gtk_builder_get_object (home_priv->builder, "update_button"));
	home_priv->loadbutton = GTK_WIDGET (gtk_builder_get_object (home_priv->builder, "load_button"));
	home_priv->settingsbutton = GTK_WIDGET (gtk_builder_get_object (home_priv->builder, "settings_button"));	
	


	home_priv->time_label = GTK_WIDGET(gtk_builder_get_object(home_priv->builder, "time_label"));
	/* Add a grid to the scrolled window */
    home_priv->grid=gtk_grid_new();
	gtk_widget_set_vexpand((GtkWidget *)home_priv->grid,TRUE);
    gtk_container_add (GTK_CONTAINER(home_priv->scrollwindow), home_priv->grid);
    /* Add a row for each entry */
	for(int i=0;i<NUM_ENTRIES;i++){
        home_priv->status_label[i]=gtk_label_new(status_table[i].name);
        gtk_label_set_width_chars (GTK_LABEL(home_priv->status_label[i]),strlen(status_table[i].name));
        gtk_label_set_max_width_chars (GTK_LABEL(home_priv->status_label[i]),NAME_CHARS);
        gtk_widget_set_size_request (home_priv->status_label[i],120,38);

        home_priv->status_val[i]=gtk_label_new(status_table[i].status);
        gtk_label_set_width_chars (GTK_LABEL(home_priv->status_val[i]),strlen(status_table[i].status));
        gtk_label_set_max_width_chars (GTK_LABEL(home_priv->status_val[i]),VAL_CHARS);

		/* attach the label and status bar for each load to the corresponding row in the grid */
		if (!status_table[i].levelbar){
    		gtk_grid_attach ((GtkGrid*)home_priv->grid, home_priv->status_label[i],0,i,1,1);
    		gtk_grid_attach ((GtkGrid*)home_priv->grid, home_priv->status_val[i],1,i,1,1);
		}
		else
		{
			home_priv->levelbargrid=gtk_grid_new();
			gtk_grid_set_column_spacing((GtkGrid*)home_priv->levelbargrid,10);
			home_priv->levelbar[i]=gtk_level_bar_new ();
			gtk_orientable_set_orientation ((GtkOrientable *)home_priv->levelbar[i],GTK_ORIENTATION_VERTICAL);
			gtk_level_bar_set_inverted ((GtkLevelBar *)home_priv->levelbar[i],TRUE);
			gtk_grid_attach ((GtkGrid*)home_priv->levelbargrid, home_priv->status_val[i],0,0,1,1);
    		gtk_grid_attach ((GtkGrid*)home_priv->levelbargrid, home_priv->levelbar[i],1,0,1,1);
			gtk_grid_attach ((GtkGrid*)home_priv->grid, home_priv->status_label[i],0,i,1,1);
    		gtk_grid_attach ((GtkGrid*)home_priv->grid, home_priv->levelbargrid,1,i,1,1);
		}
	}

	/* Connect callbacks */
	/* By passing the pointer to the home_priv struct, any Gtk element in the home window defined in home_priv can be accessed by the called function */
    g_signal_connect (home_priv->updatebutton, "clicked",G_CALLBACK (on_update_button_clicked),(gpointer)home_priv);
    g_signal_connect (home_priv->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
	g_signal_connect (home_priv->loadbutton, "clicked",G_CALLBACK (on_load_button_clicked),(gpointer)home_priv);
	g_signal_connect (home_priv->settingsbutton, "clicked",G_CALLBACK (on_settings_button_clicked),(gpointer)home_priv);


	/* Initialize values of widgets */
	update_and_print_status((gpointer)home_priv);

	/* Start Periodic Updates */
	home_priv->timeout_tag=g_timeout_add(UPDATE_INTERVAL_MS, (GSourceFunc)update_and_print_status, (gpointer)home_priv);


	/* -----------------CSS----------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /*--------------------------------------------------------------------------------------------------------*/
    
   
	/* Destroy builder */
	g_object_unref (home_priv->builder);

	
	return home_priv->window;
}





