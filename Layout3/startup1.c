/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */ /*
 * startup.c
 * Copyright (C) 2017 David Sehloff <dsehloff@wisc.edu>
 * 
 * hem-gui is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hem-gui is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <glib/gi18n.h>


#include "startup.h"
#include "home_screen.h"
//#include "../include/loads_screen.h"


GtkEntryBuffer *timezone_buffer;
GtkEntryBuffer *dc_buffer;
GtkEntryBuffer *hz_buffer;
GtkEntryBuffer *phone_num_buffer;
char userNumber[25];
char displayNumber[25];

//TEMPORARY for testing autopopulation of entries
int phoneDefined = 1;
int zoneDefined = 0;
int dcDefined = 0;
int hzDefined = 1;


/* -------------specific save functions for entry values--------------- */


	
/* -------------------------------------------------------------------- */


/*
 * Function:  on_save_and_continue_clicked
 * --------------------
 * To be called when the save and exit button is clicked,
 * calls the functions to save each of the values currently in the entry boxes,
 * then switches the screen to the home screen
 * by hiding the settings screen, freeing its variables, and presenting the home window
 */
int
on_save_and_continue_clicked (GtkWidget *widget, gpointer data)
{
	int success = 0;
	const gchar *entered_tz_text = gtk_entry_get_text ((GtkEntry*)((StartupPrivate*)data)->timezone_entry);
	//int netstatus = save_tz(entered_tz_text);
	const gchar *entered_num_text = gtk_entry_get_text ((GtkEntry*)((StartupPrivate*)data)->phone_num_entry);
	//int numstatus = save_phone(entered_num_text);
	//if (netstatus && passstatus && numstatus)
	//{
	//	success = 1;
	//}
	gtk_window_present((GtkWindow*)home_priv->window);
	gtk_widget_hide(GTK_WIDGET(((StartupPrivate*)data)->window));
	g_source_remove ((gint)((StartupPrivate*)data)->timeout_tag);
	g_free ((StartupPrivate*)data);	
	
	return success;
}

	
GtkWidget*
create_startup_window (void)
{
    /*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    //GtkStyleContext *context;
    /*---------------------------*/

	char *init_timezone = "Asia/Kolkata";
	//char *init_password = get_current_password();


	timezone_buffer=gtk_entry_buffer_new(init_timezone,strlen(init_timezone));
	//wifi_pass_buffer=gtk_entry_buffer_new(init_password,strlen(init_password));
	phone_num_buffer=gtk_entry_buffer_new(displayNumber,strlen(userNumber));

	/* Get memory for structure of this window's widgets */
	startup_priv = g_malloc (sizeof (struct Startup_Private));

	GError* error = NULL;

	/* Load UI from file */
	startup_priv->builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (startup_priv->builder, UI_FILE_STARTUP, &error))
	{
		g_critical ("Couldn't load builder file: %s", error->message);
		g_error_free (error);
	}

	/* Get the window object from the ui file */
	startup_priv->window = GTK_WIDGET (gtk_builder_get_object (startup_priv->builder, STARTUP_WINDOW));
        if (!startup_priv->window)
        {
                g_critical ("Widget \"%s\" is missing in file %s.",
				STARTUP_WINDOW,
				UI_FILE_STARTUP);
        }
	startup_priv->scrollwindow = (GtkScrolledWindow*)(gtk_builder_get_object(startup_priv->builder, "scrolledwindow"));
	gtk_widget_set_vexpand((GtkWidget *)startup_priv->scrollwindow,TRUE);
	startup_priv->exitbutton = GTK_WIDGET (gtk_builder_get_object (startup_priv->builder, "exit_button"));
	startup_priv->save_and_exitbutton = GTK_WIDGET (gtk_builder_get_object (startup_priv->builder, "save_exit_button"));
    /* Add a grid to the scrolled window */
    startup_priv->grid=gtk_grid_new();
    gtk_grid_set_row_spacing ((GtkGrid*)(startup_priv->grid),7);
    gtk_grid_set_column_spacing ((GtkGrid*)(startup_priv->grid),5);
    gtk_container_add (GTK_CONTAINER(startup_priv->scrollwindow), startup_priv->grid);

    int i = 1;
	if(!phoneDefined){
    	startup_priv->phone_num_label=gtk_label_new("User\nPhone #");
    	startup_priv->phone_num_entry=gtk_entry_new_with_buffer (phone_num_buffer);

    	/* attach the label and entry box to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->phone_num_label,0,i,1,1);
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->phone_num_entry,1,i,1,1);
        i++;
    }
    
    if(!zoneDefined){
        startup_priv->timezone_label=gtk_label_new("Timezone");
	    startup_priv->timezone_entry=gtk_entry_new_with_buffer (timezone_buffer);

	    /* attach the label and entry box to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->timezone_label,0,i,1,1);
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->timezone_entry,1,i,1,1);
        i++;
    }

    if(!dcDefined){
        GtkWidget *dcBox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
        gtk_box_set_homogeneous (GTK_BOX (dcBox), TRUE);

	    startup_priv->dc_label=gtk_label_new("System Type");
		
	    startup_priv->dc_entry=gtk_entry_new ();

    	startup_priv->dcRadio = gtk_radio_button_new_with_label (NULL,"DC");
        //gtk_container_add (GTK_CONTAINER (startup_priv->dcRadio), startup_priv->dc_entry);
	    startup_priv->acRadio = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (startup_priv->dcRadio),"AC");

        gtk_box_pack_start (GTK_BOX (dcBox), startup_priv->dcRadio, TRUE, TRUE, 2);
        gtk_box_pack_start (GTK_BOX (dcBox), startup_priv->acRadio, TRUE, TRUE, 2);


    	/* attach the label and entry box to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->dc_label,0,i,1,1);
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, dcBox,1,i,1,1);
        i++;
    }

	if(!hzDefined){
        GtkWidget *hzBox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
        gtk_box_set_homogeneous (GTK_BOX (hzBox), TRUE);

	    startup_priv->hz_label=gtk_label_new("System Frequency");
	    startup_priv->hz_entry=gtk_entry_new_with_buffer (hz_buffer);

    	startup_priv->radio50 = gtk_radio_button_new_with_label (NULL,"50 Hz");
        gtk_container_add (GTK_CONTAINER (startup_priv->radio60), startup_priv->hz_entry);
	    startup_priv->radio60 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (startup_priv->radio50),"60 Hz");

        gtk_box_pack_start (GTK_BOX (hzBox), startup_priv->radio50, TRUE, TRUE, 2);
        gtk_box_pack_start (GTK_BOX (hzBox), startup_priv->radio60, TRUE, TRUE, 2);


    	/* attach the label and entry box to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, startup_priv->hz_label,0,i,1,1);
        gtk_grid_attach ((GtkGrid*)startup_priv->grid, hzBox,1,i,1,1);
        i++;
	}





	
    /* Connect callbacks */
	//g_signal_connect (startup_priv->save_and_exitbutton, "clicked",G_CALLBACK (on_save_and_continue_clicked),(gpointer)startup_priv);
    g_signal_connect (startup_priv->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
	//TODO connect "toggled" signal from each radio button to radio update function
	g_signal_connect ();

	/* Initialize value of time */
	//update_startup((gpointer)startup_priv);


	/* Start Periodic Updates */
	//startup_priv->timeout_tag=g_timeout_add(UPDATE_INTERVAL_MS, (GSourceFunc)update_startup, (gpointer)startup_priv);


    /* -----------------CSS----------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /*--------------------------------------------------------------------------------------------------------*/
    
    /* For touchscreen, set fullscreen */
    //gtk_window_fullscreen ((GtkWindow*)startup_priv->window);


    /* Destroy builder */
    g_object_unref( G_OBJECT( startup_priv->builder ) );

	return startup_priv->window;
}




