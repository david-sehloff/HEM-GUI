/* struct to store all the Gtk elements specific to the settings screen */

#ifndef _STARTUPH_
#define _STARTUPH_

#define UI_FILE_STARTUP "startup.ui"
#define STARTUP_WINDOW "startup_window"

typedef struct Startup_Private StartupPrivate;
struct Startup_Private
{
	GtkWidget *window;
	GtkBuilder *builder;
	GtkScrolledWindow *scrollwindow;
	GtkWidget *exitbutton;
	GtkWidget *save_and_exitbutton;
	GtkWidget *grid;
	GtkWidget *time_label;
	GtkWidget *phone_num_label;
	GtkWidget *phone_num_entry;
	GtkWidget *timezone_label;
	GtkWidget *timezone_combo_box;
    GtkWidget *dcRadio;
    GtkWidget *acRadio;
	GtkWidget *dc_label;
	GtkWidget *dc_entry;
    GtkWidget *radio50;
    GtkWidget *radio60;
	GtkWidget *hz_label;
	GtkWidget *hz_entry;
    GtkWidget *hz_box;
    gint timeout_tag;
	
};


StartupPrivate* startup_priv;

int
update_startup(gpointer loads_priv);

void
on_exit_button_clicked (GtkWidget *widget, gpointer data);

char * 
get_current_network(void);

char *
get_current_password(void);

char *
get_current_phone(void);

int
save_network (const gchar *value);

int
save_password (const gchar *value);

int
save_phone (const gchar *value);

GtkWidget* create_settings_window (void);

#endif
