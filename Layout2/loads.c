#include "loads.h"

//global struct for each node's meter values
struct node_data{
  float voltage;
  float current;
  float power;
  bool on;
};
//struct for time
struct current_time{
  int hour;
  int minute;
};
bool relayON[NUM_LOADS];

//global Gtk Variables for each widget
GtkWidget *status_load[NUM_LOADS];

GtkWidget *time_label;
guint context_id[NUM_LOADS];



GtkWidget *relay_toggle[NUM_LOADS];
//globally define a specific instance for each load
struct node_data load[NUM_LOADS];
struct current_time time_val;
/*
 * Function:  get_status_data 
 * --------------------
 * updates the struct data with the current status of each load
 * to display on the status screen **static example for now
 */
void get_meter_data(void)
{
  for(int i=0;i<NUM_LOADS;i++){
    load[i].voltage=12;
    load[i].current=1+i;
    load[i].power=load[i].voltage*load[i].current;
    load[i].on=relayON[i];
  }
  time_val.hour=7;
  time_val.minute=0;
}

/*
 * Function:  push_status
 * --------------------
 * prints meaningful verbal representations of the current status variables to the statusbars
 * static because of updating count, which is used only for testing
 */
static void print_data(void)
{

  gchar *buff;
  gchar *hourstr;
  gchar *minutestr;
  static int count = 0;
  time_val.minute = (++count) % 60;

  /* push to each load's power statusbar */
  for(int i=0;i<NUM_LOADS;i++){
    buff = g_strdup_printf ("%.2lf W",load[i].power);
    gtk_label_set_text (GTK_LABEL (status_load[i]),buff);
    //gtk_statusbar_push (GTK_STATUSBAR (status_load[i]), context_id[i], buff);
    g_free (buff);
  }


  /*-----------------update time label---------------*/
  if (time_val.hour<10){
    hourstr = g_strdup_printf ("0%d",time_val.hour);
  }
  else {
    hourstr = g_strdup_printf ("%d",time_val.hour);
  }
  if (time_val.minute<10){
    minutestr = g_strdup_printf ("0%d",time_val.minute);
  }
  else {
    minutestr = g_strdup_printf ("%d",time_val.minute);
  }
  buff = g_strdup_printf ("%s:%s",hourstr,minutestr);
  g_free (hourstr);
  g_free (minutestr);
  gtk_label_set_text (GTK_LABEL (time_label),buff);
  g_free (buff);
  /*--------------------------------------------------*/


}


void relay_update(void)
{
    GtkStyleContext *context;

    for (int i=0;i<NUM_LOADS;i++){
        /*CSS*/        
        context = gtk_widget_get_style_context(relay_toggle[i]);
        if (load[i].on){
            //gtk_toggle_button_set_active((GtkToggleButton*)(relay_toggle[i]),TRUE);
            gtk_button_set_label(GTK_BUTTON(relay_toggle[i]),"ON");
            /*CSS*/
            gtk_style_context_remove_class (context,"RelayOFF");
            gtk_style_context_add_class(context,"RelayON");
        }
        else {
            //gtk_toggle_button_set_active((GtkToggleButton*)(relay_toggle[i]),FALSE);
            gtk_button_set_label(GTK_BUTTON(relay_toggle[i]),"OFF");
            /*CSS*/
            gtk_style_context_remove_class (context,"RelayON");
            gtk_style_context_add_class(context,"RelayOFF");
        }
    }
}



/*
 * Function:  update_data
 * --------------------
 * pop and push to remove the previous status value from each statusbar
 * and replace it with a new one
 */
void update_data(void)
{  
  get_status_data();
  print_data();
  relay_update();
}

void relay_toggled(GtkWidget *toggle, gpointer data)
{
    int i = GPOINTER_TO_INT (data);
    relayON[i]=!relayON[i];
    //printf("relay %d: %s\n",i, relayON[i] ? "ON" : "OFF");    //debugging
    update_data();
    //printf("relay %d variable: %s\n",i, load[i].on ? "ON" : "OFF");     //debugging
}

int
load_window(GtkWidget *old_window)
//main( int    argc,
//      char **argv )
{
    GtkBuilder *builder;
    GtkWidget *window;
    GtkWidget *button;
    GtkScrolledWindow *scrollwindow;
    GtkWidget *grid;
    GtkWidget *label_load[NUM_LOADS];
    GtkWidget *relay_toggle_box;


    /*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    GtkStyleContext *context;
    /*---------------------------*/

    //set the name of each load for the label
    char load_names[NUM_LOADS][LOAD_CHARS+1];
    for(int i=0;i<NUM_LOADS;i++){
        sprintf(load_names[i],"Load %d",i+1);
    };

    //gtk_init( &argc, &argv );  
    /* Create builder and load interface */
    builder = gtk_builder_new();
    gtk_builder_add_from_file( builder, "layout2.glade", NULL );

    /* Obtain widgets that we need */
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window2"));
    button = GTK_WIDGET(gtk_builder_get_object(builder, "update_button2"));
    scrollwindow = (GtkScrolledWindow*)(gtk_builder_get_object(builder, "scrolledwindow1"));
    time_label = GTK_WIDGET(gtk_builder_get_object( builder, "label24"));

    /* Add a grid to the scrolled window */
    grid=gtk_grid_new();
    gtk_container_add (GTK_CONTAINER(scrollwindow), grid);
    for(int i=0;i<NUM_LOADS;i++){
        label_load[i]=gtk_label_new(load_names[i]);
        gtk_label_set_width_chars (GTK_LABEL(label_load[i]),LOAD_CHARS);
        gtk_label_set_max_width_chars (GTK_LABEL(label_load[i]),LOAD_CHARS);
        gtk_widget_set_size_request (label_load[i],60,40);

        status_load[i]=gtk_label_new(load_names[i]);
        gtk_label_set_width_chars (GTK_LABEL(status_load[i]),PWR_CHARS);
        gtk_label_set_max_width_chars (GTK_LABEL(status_load[i]),PWR_CHARS);
        
        //context_id[i] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load[i]), "Power");
        relay_toggle_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
        relay_toggle[i] = gtk_toggle_button_new_with_label("OFF");
        gtk_toggle_button_set_mode((GtkToggleButton*)(relay_toggle[i]),FALSE);
        gtk_container_add (GTK_CONTAINER(relay_toggle_box),relay_toggle[i]);
        gtk_widget_set_size_request (relay_toggle[i],40,35);
        gtk_widget_set_size_request (relay_toggle_box,40,40);

        /*--CSS--*/
        context = gtk_widget_get_style_context(label_load[i]);
        gtk_style_context_add_class(context,"LoadLabel");
        context = gtk_widget_get_style_context(status_load[i]);
        gtk_style_context_add_class(context,"LoadStatus");
        context = gtk_widget_get_style_context(relay_toggle[i]);
        if (load[i].on){
            gtk_style_context_add_class(context,"RelayON");
        }
        else {
            gtk_style_context_add_class(context,"RelayOFF");
        }
        /*-------*/

        /* attach the label and status bar for each load to the corresponding row in the grid */
        gtk_grid_attach ((GtkGrid*)grid, label_load[i],0,i,1,1);
        gtk_grid_attach ((GtkGrid*)grid, status_load[i],2,i,1,1);
        gtk_grid_attach ((GtkGrid*)grid, relay_toggle_box,1,i,1,1);
        /* Connect callbacks */
        g_signal_connect (relay_toggle[i], "toggled", G_CALLBACK (relay_toggled),GINT_TO_POINTER(i));
    }
    /* Connect callbacks */
    g_signal_connect (button, "clicked",G_CALLBACK (update_data), NULL);
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);


    /* -----------------CSS----------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    GError *error = 0;
 
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /*--------------------------------------------------------------------------------------------------------*/
    
    
    /* Destroy builder */
    g_object_unref( G_OBJECT( builder ) );

    /* Show window and hide old */
    gtk_widget_hide(old_window);
    gtk_widget_show_all( window );

    /* For touchscreen only */
    //gtk_window_fullscreen (window);
    //gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(gtk_window)),gdk_cursor_new(GDK_BLANK_CURSOR));
    

    //gtk_main();
    return( 0 );
}
